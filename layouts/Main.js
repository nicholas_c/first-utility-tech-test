import Meta from './../components/layout/Meta'
import Header from './../components/layout/Header'
import './../src/scss/styles.scss'

export default ({ children }) => (
  <div>
    <Meta />
    <Header />

    <div className="container">
      { children }
    </div>
  </div>
)
