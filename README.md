# First Utility Star wars API Tech test

## URLs
**Homepage:** https://firstutility-starwars.herokuapp.com/

**Custom error page:** https://firstutility-starwars.herokuapp.com/this-is-an-error-page

**Example category page:** https://firstutility-starwars.herokuapp.com/people

**Example search query:** https://firstutility-starwars.herokuapp.com/search?query=r2

**Example search query (No results):** https://firstutility-starwars.herokuapp.com/search?query=this-is-something

## Overview
The Application is built on Next.js, with React as the framework of choice and Server Side Rendering enabled.

Although the project spec called for just "people" to be shown, it was as easy and took next to no time to just implement all endpoints from the Star Wars API and drop in simple pagination.

On top of doing the extra pages and pagination, I felt that images were required from an FE point of view, this has made the Cards component a little messier, as there is a lot of "hacks" in to get images in place. However the end result is that each category has random related images. People have random character images (With the exception of some matched names) and Films all have the correct movie posters.

Search was also implemented as this was simply drop in once the rest of the pages were complete, however does only search people.

### Time breakdown:
**Next.js and React setup:** ~45minutes

**Styling:** Sass loader Installation and base header/styling ~45hr

**Card Styling:** ~15 minutes

**Image "hacks":** ~20 minutes

**getInitialProps Data fetching:** ~40 minutes

**Pagination:** 15 minutes


## Future FE and API improvements
### FE Improvements
Given time contraints I didn't have time to drop in unit tests for components, so that'd be one of the first things to complete next.

API doesn't let search you all endpoints, so only "people" is dropped in at the moment, I started to explore using Promise.all, or async/await to go and do the 6 required API calls to get all the data, as search results, but this was deemed to costly in time to do at this time.
