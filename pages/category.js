import React from 'react'
import fetch from 'isomorphic-unfetch'
import Link from 'next/link'

import Page from './../layouts/Main'
import Card from './../components/Card'
import Pagination from './../components/Pagination'

export default class Category extends React.Component {
  constructor(props) {
    super(props)
  }

  static async getInitialProps({req, asPath}) {
    const res = await fetch(`http://swapi.co/api${asPath}`)
    const data = await res.json()

    return {
      currentPage: parseInt(req.query.page ? req.query.page : 1),
      count: data.count,
      type: asPath.split('?')[0],
      items: data.results
    }
  }

  render() {
    return (
      <Page>
        <div className="results">
          {this.props.items.map((item) => (
            <Card item={item} type={this.props.type.replace(/\//g, '')} key={item.name} />
          ))}
        </div>

        <Pagination count={this.props.count} currentPage={this.props.currentPage} />
      </Page>
    )
  }
}
