import Page from '../layouts/Main'
import Card from '../components/Card'
import './../src/scss/components/intro.scss'

export default () => (
  <Page>
    <div className="intro">
      <img src="https://media.giphy.com/media/bq6F8QYqBU7Yc/giphy.gif" />

      <h2 className="intro__text">
        These aren't the droids you're looking for...
      </h2>
    </div>
  </Page>
)
