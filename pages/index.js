import Page from '../layouts/Main'
import Card from '../components/Card'
import './../src/scss/components/intro.scss'

export default () => (
  <Page>
    <div className="intro">
      <img src="https://media.giphy.com/media/3ornk57KwDXf81rjWM/giphy.gif" />
      <img src="https://media.giphy.com/media/Nx0rz3jtxtEre/giphy.gif" />

      <h2 className="intro__text">
        Select a category, or type in the search box to search for characters.
      </h2>
    </div>
  </Page>
)
