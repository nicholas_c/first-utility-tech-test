import React from 'react'
import fetch from 'isomorphic-unfetch'

import Page from '../layouts/Main'
import Card from '../components/Card'
import Pagination from './../components/Pagination'
import './../src/scss/components/intro.scss'

export default class Category extends React.Component {
  constructor(props) {
    super(props)
  }

  static async getInitialProps({req, asPath}) {
    const res = await fetch(`http://swapi.co/api/people?search=${req.query.query}`)
    const data = await res.json()

    return {
      currentPage: parseInt(req.query.page ? req.query.page : 1),
      count: data.count,
      type: '/people',
      query: req.query.query,
      items: data.results
    }
  }

  render() {
    return (
      <Page>
        <div className="results">
          {this.props.items && this.props.items.map((item) => (
            <Card item={item} type={this.props.type.replace(/\//g, '')} key={item.name} />
          ))}

          {this.props.items.length === 0 &&

            <div className="intro">
              <img src="https://media.giphy.com/media/e2qy9vqxWl9Ju/giphy.gif" />

              <p className="intro__text">
                No search results found for "{this.props.query}"
              </p>
            </div>
          }
        </div>

        <Pagination count={this.props.count} currentPage={this.props.currentPage} />
      </Page>
    )
  }
}
