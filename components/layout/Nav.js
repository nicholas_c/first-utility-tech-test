import React from 'react'
import Link from 'next/link'
import { FaHome, FaMale, FaFilm, FaSpaceShuttle, FaShuttleVan, FaRedditAlien, FaGlobeAfrica } from 'react-icons/fa'

export default class Navigation extends React.Component {
  constructor(props) {
    super(props)

    this.navigation = [
      {
        url: '/',
        icon: <FaHome />,
        text: 'Home'
      },
      {
        url: '/people',
        icon: <FaMale />,
        text: 'People'
      },
      {
        url: '/starships',
        icon: <FaSpaceShuttle />,
        text: 'Starships'
      },
      {
        url: '/vehicles',
        icon: <FaShuttleVan />,
        text: 'Vehicles'
      },
      {
        url: '/species',
        icon: <FaRedditAlien />,
        text: 'Species'
      },
      {
        url: '/planets',
        icon: <FaGlobeAfrica />,
        text: 'Planets'
      },
      {
        url: '/films',
        icon: <FaFilm />,
        text: 'Films'
      }
    ]
  }

  render() {
    return (
      <nav className="navigation">
        {this.navigation.map((item) => (
          <Link href={item.url} key={item.text}>
            <a className="navigation__item">
              {item.icon}

              {item.text}
            </a>
          </Link>
        ))}
      </nav>
    )
  }
}
