import Logo from './Logo'
import Nav from './Nav'

const Header = () => (
  <header className="header">
    <Logo />

    <Nav />
  </header>
)

export default Header
