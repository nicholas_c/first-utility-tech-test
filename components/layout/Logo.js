import Head from 'next/head'
import Link from 'next/link'

const Logo = () => (
  <div className="w-full  header__logo">
    <Link href="/">
      <a className="header__logo__img">
        <img src="/static/sw_logo.png" />
      </a>
    </Link>

    <form action="/search" method="GET">
      <input name="query" type="search" placeholder="Search for People" />
    </form>
  </div>
)

export default Logo
