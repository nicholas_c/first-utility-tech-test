import React from 'react'
import Link from 'next/link'

export default class Pagination extends React.Component {
  render() {
    const items = []

    for (let i = 0; i < (this.props.count / 10); i++) {
      items.push(
        <Link href={`?page=${i + 1}`} key={i}>
          <a className={`results__nav__item  ${this.props.currentPage === (i + 1) ? 'results__nav__item--active' : ''}`}>
            {i + 1}
          </a>
        </Link>
      )
    }

    return (
      <div className="results__nav">
        {
          this.props.currentPage !== 1 &&

          <Link href={`?page=${this.props.currentPage - 1}`}>
            <a className="results__nav__item">
              Prev
            </a>
          </Link>
        }

        {items}

        {
          (this.props.count / 10) > this.props.currentPage &&

          <Link href={`?page=${this.props.currentPage + 1}`}>
            <a className="results__nav__item">
              Next
            </a>
          </Link>
        }
      </div>
    )
  }
}
