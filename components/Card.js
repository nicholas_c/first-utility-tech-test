import React from 'react'
import Link from 'next/link'

export default class Card extends React.Component {
  constructor(props) {
    super(props)

    switch (props.type) {
      case 'people':
        const imageHack = {
          'Luke Skywalker': 'luke',
          'C-3PO': 'c3po',
          'R2-D2': 'r2d2',
          'Darth Vader': 'darthvader',
          'Leia Organa': 'leia',
          'Obi-Wan Kenobi': 'obiwan',
          'Chewbacca': 'chewbacca',
          'Anakin Skywalker': 'anakin',
          'BB8': 'bb8',
          'Darth Maul': 'darthmaul'
        }

        // this.image = `http://www.facetheforce.today/random/400?r=${Math.floor((Math.random() * 10000) + 1)}`
        this.image =`http://www.facetheforce.today/${imageHack[props.item.name] ? imageHack[props.item.name] : 'random'}/400?r=${Math.floor((Math.random() * 10000) + 1)}`

        this.info = [
          `<strong>Height:</strong> ${props.item.height}kg`,
          `<strong>Hair Colour:</strong> ${props.item.hair_color}`,
          `<strong>Eye Colour:</strong> ${props.item.eye_color}`,
          `<strong>Skin Colour:</strong> ${props.item.skin_color}`,
          `<strong>Birth Year:</strong> ${props.item.birth_year}`,
          `<strong>Gender:</strong> ${props.item.gender}`
        ]
        break;
      case 'starships':
        this.image = `https://loremflickr.com/320/320/spaceship?r=${Math.floor((Math.random() * 10000) + 1)}`
        this.info = [
          `<strong>Model:</strong> ${props.item.model}`,
          `<strong>Manufacturer:</strong> ${props.item.manufacturer}`,
          `<strong>Price (In credits):</strong> ${props.item.cost_in_credits}`,
          `<strong>Crew capacity:</strong> ${props.item.crew}`,
          `<strong>Passenger capacity:</strong> ${props.item.passengers}`,
          `<strong>Hyperdrive rating:</strong> ${props.item.hyperdrive_rating}`
        ]
        break;
      case 'vehicles':
        this.image = `https://loremflickr.com/320/320/vehicle?r=${Math.floor((Math.random() * 10000) + 1)}`
        this.info = [
          `<strong>Model:</strong> ${props.item.model}`,
          `<strong>Manufacturer:</strong> ${props.item.manufacturer}`,
          `<strong>Price (In credits):</strong> ${props.item.cost_in_credits}`,
          `<strong>Crew capacity:</strong> ${props.item.crew}`,
          `<strong>Passenger capacity:</strong> ${props.item.passengers}`
        ]
        break;
      case 'species':
        this.image = `https://loremflickr.com/320/320/species?r=${Math.floor((Math.random() * 10000) + 1)}`
        this.info = [
          `<strong>Average height:</strong> ${props.item.average_height}`,
          `<strong>Skin colours:</strong> ${props.item.skin_colors}`,
          `<strong>Hair colours:</strong> ${props.item.hair_colors}`,
          `<strong>Eye colours:</strong> ${props.item.eye_colors}`,
          `<strong>Average lifespan:</strong> ${props.item.average_lifespan}`,
          `<strong>Language:</strong> ${props.item.language}`
        ]
        break;
      case 'planets':
        this.image = `https://spaceholder.cc/600?r=${Math.floor((Math.random() * 10000) + 1)}`
        this.info = [
          `<strong>Climate:</strong> ${props.item.climate}`,
          `<strong>Population:</strong> ${props.item.population}`,
          `<strong>Terrain:</strong> ${props.item.terrain}`,
          `<strong>Gravity:</strong> ${props.item.gravity}`,
          `<strong>Diameter:</strong> ${props.item.diameter}`,
          `<strong>Days in a year:</strong> ${props.item.orbital_period}`,
          `<strong>Hours in a day:</strong> ${props.item.rotation_period}`
        ]
        break;
      case 'films':
        switch (props.item.episode_id) {
          case (1):
            this.image = 'https://images-na.ssl-images-amazon.com/images/I/71XrR%2BOj6gL._SY550_.jpg'
            break;
          case (2):
            this.image = 'https://upload.wikimedia.org/wikipedia/en/thumb/3/32/Star_Wars_-_Episode_II_Attack_of_the_Clones_%28movie_poster%29.jpg/220px-Star_Wars_-_Episode_II_Attack_of_the_Clones_%28movie_poster%29.jpg'
            break;
          case (3):
            this.image = 'https://images-na.ssl-images-amazon.com/images/I/71MKj4j-isL._SY679_.jpg'
            break;
          case (4):
            this.image = 'https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg'
            break;
          case (5):
            this.image = 'https://upload.wikimedia.org/wikipedia/en/thumb/3/3c/SW_-_Empire_Strikes_Back.jpg/220px-SW_-_Empire_Strikes_Back.jpg'
            break;
          case (6):
            this.image = 'https://i.pinimg.com/originals/27/11/4e/27114ed141e95e152a83ee17ad9014ad.jpg'
            break;
          case (7):
            this.image = 'https://images-na.ssl-images-amazon.com/images/I/71rZtELyYzL._SY679_.jpg'
            break;
          default:
            this.image = 'https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg'
            break;
        }

        this.info = [
          `Episode ${props.item.episode_id}`,
          `<strong>Director:</strong> ${props.item.director}`,
          `<strong>Release date:</strong> ${props.item.release_date}`
        ]
        break;
    }

    this.info = this.info.join('<br>')
  }

  render() {
    return (
      <article className="card" type={this.props.type}>
        <div className="card__inner">
          <div className="card__img">
            <img src={this.image} />
          </div>

          <div className="card__content">
            <h2 className="card__title">
              {this.props.type === 'films' ? this.props.item.title : this.props.item.name}
            </h2>

            <p dangerouslySetInnerHTML={{__html: this.info}}></p>

            <div className="card__categories">
              <div className="card__category">
                {this.props.type.charAt(0).toUpperCase()}{this.props.type.substring(1)}
              </div>
            </div>
          </div>
        </div>
      </article>
    )
  }
}
